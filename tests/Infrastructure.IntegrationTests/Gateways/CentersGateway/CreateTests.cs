﻿using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Infrastructure.Persistence;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Infrastructure.IntegrationTests.Gateways.CentersGateway
{
    public class CreateTests
    {
        [Test]
        public void Test1()
        {
            using var scope = Testing.ScopeFactory.CreateScope();
            var centersGateway = scope.ServiceProvider.GetService<ICentersGateway>();
            var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
            var user = new UserItem { Email = "Create@CentersGateway", UserName = "CreateCentersGateway", Password = "CreateCentersGateway" };
            context.Users.Add(user);
            context.SaveChanges();
            var center = new CenterItem { Title = "CreateCenter1" };
            centersGateway.Create(center, user);
            var userCenters = centersGateway.GetRolesCentersByUser(user);
            Assert.IsNotNull(user.Id);
            Assert.AreEqual(user.Id, userCenters.Value.ElementAt(0).OwnerUserId);
            Assert.AreEqual(user.Id, userCenters.Value.ElementAt(0).CreatedByUserId);

            context.Users.Remove(user);
            centersGateway.Delete(center);
            context.SaveChanges();
        }
    }
}
