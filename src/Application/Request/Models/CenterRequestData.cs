﻿using EsmoServer.Application.Request.Commands.CreateCenterRequest;
using EsmoServer.Application.Request.Commands.UpdateCenterRequest;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Request.Models
{
    public class CenterRequestData
    {
        public string Title { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Site { get; set; }
        public string Comment { get; set; }

        public static CenterRequestData FromCreateCenterRequestCommand(CreateCenterRequestCommand request)
        {
            return new CenterRequestData
            {
                Title = request.Title,
                Address = request.Address,
                Comment = request.Comment,
                Phone = request.Phone,
                Site = request.Site
            };
        }
        public static CenterRequestData FromEditCenterRequestCommand(UpdateCreateCenterRequestCommand request)
        {
            return new CenterRequestData
            {
                Title = request.Title,
                Address = request.Address,
                Comment = request.Comment,
                Phone = request.Phone,
                Site = request.Site
            };
        }
    }
}
