﻿using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Application.Request.Models;
using EsmoServer.Application.Request.Validators;
using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Domain.ValueObjects;
using FluentValidation;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Request.Commands.CreateCenterRequest
{
    public class CreateCenterRequestCommand : IRequest
    {
        public string Title { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Site { get; set; }
        public string Comment { get; set; }
    }
    public class CreateCenterRequestCommandHandler : IRequestHandler<CreateCenterRequestCommand>
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly ICentersGateway _centersGateway;
        private readonly IRequestsGateway _requestsGateway;
        public CreateCenterRequestCommandHandler(ICurrentUserService currentUserService, IRequestsGateway requestsGateway, ICentersGateway centersGateway)
        {
            _currentUserService = currentUserService;
            _requestsGateway = requestsGateway;
            _centersGateway = centersGateway;
        }
        public Task<Unit> Handle(CreateCenterRequestCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser();
            if (user is null) throw new UnauthorizedAccessException();
            var requestData = CenterRequestData.FromCreateCenterRequestCommand(request);
            //requestData.Validate();
            CenterRequestDataValidator centerRequestDataValidator = new CenterRequestDataValidator();
            centerRequestDataValidator.ValidateAndThrow(requestData);

            _requestsGateway.Create(
                new RequestItem { 
                    Data = JsonConvert.SerializeObject(requestData),
                    RequestedByUserId = user.Id, 
                    Type = RequestType.CreateCenter, 
                    Status = RequestStatus.Pending
                });
            //_centersGateway.Create(new CenterItem { Title = request.Title }, user);
            return Unit.Task;
        }
    }
}
