﻿using EsmoServer.Application.Common.Exceptions;
using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Application.Request.Models;
using EsmoServer.Application.Request.Validators;
using EsmoServer.Domain.Interfaces.Gateways;
using FluentValidation;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Request.Commands.UpdateCenterRequest
{
    public class UpdateCreateCenterRequestCommand : IRequest
    {
        public string Id { get; set; }

        public string Title { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Site { get; set; }
        public string Comment { get; set; }
    }
    public class UpdateCenterRequestCommandHandler : IRequestHandler<UpdateCreateCenterRequestCommand>
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly ICentersGateway _centersGateway;
        private readonly IRequestsGateway _requestsGateway;

        public UpdateCenterRequestCommandHandler(IRequestsGateway requestsGateway, ICurrentUserService currentUserService, ICentersGateway centersGateway)
        {
            _requestsGateway = requestsGateway;
            _currentUserService = currentUserService;
            _centersGateway = centersGateway;
        }

        public Task<Unit> Handle(UpdateCreateCenterRequestCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser();
            if (user is null) throw new UnauthorizedAccessException();
            var requestData = CenterRequestData.FromEditCenterRequestCommand(request);
            CenterRequestDataValidator centerRequestDataValidator = new CenterRequestDataValidator();
            centerRequestDataValidator.ValidateAndThrow(requestData);
            //requestData.Validate();

            var requestItem = _requestsGateway.Get(Guid.Parse(request.Id));
            if(requestItem == null)
            {
                throw new NotFoundException();
            }
            if(requestItem.RequestedByUserId != user.Id)
            {
                throw new ForbiddenAccessException();
            }
            requestItem.UpdatedAt = DateTime.UtcNow;
            requestItem.Data = JsonConvert.SerializeObject(requestData);
            _requestsGateway.SaveChanges();
            return Unit.Task;
        }
    }
}
