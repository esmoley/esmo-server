﻿using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Application.Request.Models;
using EsmoServer.Domain.Filters;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Domain.ValueObjects;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Request.Queries.GetCreateCenterRequests
{
    public class GetCreateCenterRequestsQuery : IRequest<GetCreateCenterRequestsVm>
    {
    }
    public class GetCreateCenterRequestsQueryHandler : IRequestHandler<GetCreateCenterRequestsQuery, GetCreateCenterRequestsVm>
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IRequestsGateway _requestsGateway;

        public GetCreateCenterRequestsQueryHandler(IRequestsGateway requestsGateway, ICurrentUserService currentUserService)
        {
            _requestsGateway = requestsGateway;
            _currentUserService = currentUserService;
        }

        public async Task<GetCreateCenterRequestsVm> Handle(GetCreateCenterRequestsQuery request, CancellationToken cancellationToken)
        { 
            var user = _currentUserService.GetUser();
            if (user is null) throw new UnauthorizedAccessException();
            var requestItems = _requestsGateway.Where(x => x.Type.Value == RequestType.CreateCenter.Value
                && x.RequestedByUserId == user.Id 
                && x.Status.Value == RequestStatus.Pending.Value );
            GetCreateCenterRequestsVm result = new GetCreateCenterRequestsVm { Requests = new List<GetCreateCenterRequestsDto>() };
            foreach(var requestItem in requestItems)
            {
                var resultItem = JsonConvert.DeserializeObject<GetCreateCenterRequestsDto>(requestItem.Data);
                resultItem.RequestId = requestItem.Id.ToString();
                resultItem.Status = requestItem.Status.Value;
                result.Requests.Add(resultItem);
            }
            return await Task.FromResult(result);
        }
    }
}
