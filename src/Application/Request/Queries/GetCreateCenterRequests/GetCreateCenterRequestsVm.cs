﻿using EsmoServer.Application.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Request.Queries.GetCreateCenterRequests
{
    public class GetCreateCenterRequestsVm
    {
        //public AlertVm Alert { get; set; }
        public List<GetCreateCenterRequestsDto> Requests { get; set; }
    }
}
