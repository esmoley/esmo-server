﻿using EsmoServer.Application.Request.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Request.Queries.GetCreateCenterRequests
{
    public class GetCreateCenterRequestsDto : CenterRequestData
    {
        public string RequestId { get; set; }
        public string Status { get; set; }
    }
}
