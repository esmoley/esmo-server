﻿using EsmoServer.Application.Request.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Request.Validators
{
    public class CenterRequestDataValidator : AbstractValidator<CenterRequestData>
    {
        public CenterRequestDataValidator()
        {
            RuleFor(centerRequestData => centerRequestData.Title).NotEmpty();
            RuleFor(centerRequestData => centerRequestData.Site).NotEmpty();
            RuleFor(centerRequestData => centerRequestData.Phone).NotEmpty();
            RuleFor(centerRequestData => centerRequestData.Address).NotEmpty();
        }
    }
}
