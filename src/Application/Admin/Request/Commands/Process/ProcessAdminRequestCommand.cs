﻿using EsmoServer.Application.Common.Exceptions;
using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Application.Request.Models;
using EsmoServer.Application.Request.Validators;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Domain.ValueObjects;
using FluentValidation;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Admin.Request.Commands.Process
{
    public class ProcessAdminRequestCommand : IRequest
    {
        public string Id { get; set; }
        public string Data { get; set; }
        public string Status { get; set; }
        public string StatusReason { get; set; }
    }
    public class ProcessAdminRequestCommandHandler : IRequestHandler<ProcessAdminRequestCommand>
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IRequestsGateway _requestsGateway;
        private readonly ICentersGateway _centerGateway;
        private readonly IUsersGateway _usersGateway;

        public ProcessAdminRequestCommandHandler(ICurrentUserService currentUserService, IRequestsGateway requestsGateway, ICentersGateway centerGateway, IUsersGateway usersGateway)
        {
            _currentUserService = currentUserService;
            _requestsGateway = requestsGateway;
            _centerGateway = centerGateway;
            _usersGateway = usersGateway;
        }

        public Task<Unit> Handle(ProcessAdminRequestCommand request, CancellationToken cancellationToken)
        {
            var cul = Thread.CurrentThread.CurrentCulture;
            var culUi = Thread.CurrentThread.CurrentUICulture;
            var admin = _currentUserService.GetUser();
            if (admin is null) throw new UnauthorizedAccessException();
            if (!admin.IsAdmin) throw new ForbiddenAccessException();

            if (request.Id == "") throw new Common.Exceptions.ValidationException();
            if (request.Data == "") throw new Common.Exceptions.ValidationException();

            var req = _requestsGateway.Get(Guid.Parse(request.Id));
            if (req == null) throw new Exception("Request not found");

            if (req.Status != RequestStatus.Pending) throw new Exception("Request already processed");

            var reqUser = _usersGateway.Where(x => x.Id == req.RequestedByUserId).First();
            if (reqUser == null) throw new Exception("Requested user not found");

            var requestStatus = RequestStatus.From(request.Status);

            if (!RequestStatus.GetAll().Contains(requestStatus)) throw new Exception("Status does not exists");

            if (RequestType.CreateCenter == req.Type)
            {
                var requestData = JsonConvert.DeserializeObject<CenterRequestData>(request.Data);
                CenterRequestDataValidator centerRequestDataValidator = new CenterRequestDataValidator();
                centerRequestDataValidator.ValidateAndThrow(requestData);
                //data.Validate();
                if (requestStatus == RequestStatus.Accepted)
                {
                    var center = new Domain.Entities.CenterItem
                    {
                        Id = req.Id,
                        Address = requestData.Address,
                        CreatedAt = DateTime.Now,
                        CreatedByUserId = reqUser.Id,
                        OwnerUserId = reqUser.Id,
                        Phone = requestData.Phone,
                        Site = requestData.Site,
                        Status = CenterStatus.OK,
                        Title = requestData.Title,
                        UpdatedAt = DateTime.Now
                    };
                    _centerGateway.Create(center, reqUser);
                    reqUser.CurrentCenterId = center.Id;
                }
                req.Status = requestStatus;
                req.ProcessedByAdminId = admin.Id;
                req.Data = request.Data;
                req.StatusReason = request.StatusReason;
                req.UpdatedAt = DateTime.Now;
                _requestsGateway.SaveChanges();

                return Unit.Task;
            }
            else
            {
                throw new Exception("Request type not found");
            }
        }
    }
}
