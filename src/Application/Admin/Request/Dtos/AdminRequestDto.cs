﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Admin.Request.Dtos
{
    public class AdminRequestDto
    {
        public string Id { get; set; }

        public string Data { get; set; }

        public string RequestedByUserId { get; set; }
        public UserDto User { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }
        public string StatusReason { get; set; }

        public string ProcessedByUserId { get; set; }

        public DateTime? CreatedAt { get; set; } = DateTime.Now;

        public DateTime? UpdatedAt { get; set; } = DateTime.Now;
    }
}
