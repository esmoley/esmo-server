﻿using EsmoServer.Application.Admin.Request.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Admin.Request.Queries.Get
{
    public class GetAdminRequestsVm
    {
        public IEnumerable<AdminRequestDto> Requests { get; set; }
    }
}
