﻿using EsmoServer.Application.Admin.Request.Dtos;
using EsmoServer.Application.Common.Exceptions;
using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Domain.ValueObjects;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Admin.Request.Queries.Get
{
    public class GetAdminRequestsQuery : IRequest<GetAdminRequestsVm>
    {
    }
    public class GetAdminRequestsQueryHandler : IRequestHandler<GetAdminRequestsQuery, GetAdminRequestsVm>
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IUsersGateway _usersGateway;
        private readonly IRequestsGateway _requestsGateway;
        public GetAdminRequestsQueryHandler(IRequestsGateway requestsGateway, ICurrentUserService currentUserService, IUsersGateway usersGateway)
        {
            _requestsGateway = requestsGateway;
            _currentUserService = currentUserService;
            _usersGateway = usersGateway;
        }
        public async Task<GetAdminRequestsVm> Handle(GetAdminRequestsQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser();
            if (user is null) throw new UnauthorizedAccessException();
            if (!user.IsAdmin) throw new ForbiddenAccessException();
            var requestItems = _requestsGateway.Where(x => true).OrderByDescending(x=>x.Status.Value == RequestStatus.Pending.Value).ThenByDescending((x)=>x.Id);

            var userIdRequestsMap = new Dictionary<Guid, List<AdminRequestDto>>();
            var result = new List<AdminRequestDto>();
            foreach (var requestItem in requestItems)
            {
                if (!userIdRequestsMap.TryGetValue(requestItem.RequestedByUserId, out var userRequests))
                {
                    userRequests = new List<AdminRequestDto>();
                    userIdRequestsMap.Add(requestItem.RequestedByUserId, userRequests);
                }
                var requestDto = new AdminRequestDto
                {
                    Id = requestItem.Id.ToString(),
                    CreatedAt = requestItem.CreatedAt,
                    Data = requestItem.Data,
                    ProcessedByUserId = requestItem.ProcessedByAdminId.ToString(),
                    RequestedByUserId = requestItem.RequestedByUserId.ToString(),
                    Status = requestItem.Status.Value,
                    Type = requestItem.Type.Value,
                    UpdatedAt = requestItem.UpdatedAt,
                    StatusReason = requestItem.StatusReason
                };
                result.Add(requestDto);
                userRequests.Add(requestDto);
            }
            if (result.Count > 0)
            {
                var foundUsers = _usersGateway.Where(x => userIdRequestsMap.Keys.Contains(x.Id));
                foreach(var foundUser in foundUsers)
                {
                    if(userIdRequestsMap.TryGetValue(foundUser.Id, out var userRequests))
                    {
                        var userDto = new UserDto
                        {
                            CreatedAt = foundUser.CreatedAt,
                            CurrentCenterId = foundUser.CurrentCenterId,
                            Email = foundUser.Email,
                            Id = foundUser.Id.ToString(),
                            IsAdmin = foundUser.IsAdmin,
                            PhoneNumber = foundUser.PhoneNumber,
                            UpdatedAt = foundUser.UpdatedAt,
                            UserName = foundUser.UserName
                        };
                        foreach (var userRequest in userRequests)
                        {
                            userRequest.User = userDto;
                        }
                    }
                    
                }
            }
            return new GetAdminRequestsVm { Requests = result };
        }
    }
}
