﻿using EsmoServer.Application.Common.ViewModels;
using EsmoServer.Infrastructure.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Connection.Queries
{
    public class OnConnectedQuery : IRequest
    {
        public Func<DispatcherActionVm, CancellationToken, Task> SendEvent { get; set; }
        public string AccessToken { get; set; }
        public ClaimsPrincipal User { get; set; }
    }
    public class OnConnectedQueryHandler : IRequestHandler<OnConnectedQuery>
    {
        private readonly ApplicationDbContext _dbContext;

        public OnConnectedQueryHandler(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Unit> Handle(OnConnectedQuery request, CancellationToken cancellationToken)
        {
            //await request.DispatchEvent(new EventMessageVm { Payload = request.User.Claims.FirstOrDefault(x => x.Type == "id").Value }, cancellationToken);
            return Unit.Value;
        }
    }

}
