﻿using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Interfaces.Gateways;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Center.Queries.GetCenters
{
    public class GetCentersQuery : IRequest<GetCentersVm>
    {
    }
    public class GetCentersQueryHandler : IRequestHandler<GetCentersQuery, GetCentersVm>
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly ICentersGateway _centersGateway;

        public GetCentersQueryHandler(ICurrentUserService currentUserService, ICentersGateway centersGateway)
        {
            _currentUserService = currentUserService;
            _centersGateway = centersGateway;
        }
        
        public async Task<GetCentersVm> Handle(GetCentersQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser();
            if (user is null) throw new UnauthorizedAccessException();
            var centersOwner = _centersGateway.GetRolesCentersByUser(user);
            Dictionary<Guid, CenterDto> centersMap = new Dictionary<Guid, CenterDto>();
            if (centersOwner.Value != null)
            {
                foreach(var centers in centersOwner.Value)
                {
                    centersMap.Add(centers.Id, new CenterDto
                    {
                        Id = centers.Id,
                        Roles = new List<RoleDto>(),
                        Status = centers.Status.Value,
                        Title = centers.Title,
                        IsOwner = centers.OwnerUserId == user.Id
                    });
                }
                foreach (var centerRole in centersOwner.Key)
                {
                    if(centersMap.TryGetValue(centerRole.CenterId, out var foundCenter))
                    {
                        foundCenter.Roles.Add(new RoleDto { Title = centerRole.Title });
                    }
                }
            }
            return new GetCentersVm { Centers = centersMap.Values };
        }
    }
}
