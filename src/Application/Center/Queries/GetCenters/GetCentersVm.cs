﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Center.Queries.GetCenters
{
    public class GetCentersVm
    {
        public IEnumerable<CenterDto> Centers { get; set; }
    }
}
