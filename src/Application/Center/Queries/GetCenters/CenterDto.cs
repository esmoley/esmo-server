﻿using EsmoServer.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Center.Queries.GetCenters
{
    public class CenterDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Status { get; set; } = CenterStatus.OK.Value;
        public List<RoleDto> Roles { get; set; } = new List<RoleDto>();
        public bool IsOwner { get; set; }
    }
}
