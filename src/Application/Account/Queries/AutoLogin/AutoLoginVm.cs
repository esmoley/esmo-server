﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Account.Queries.AutoLogin
{
    public class AutoLoginVm
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public Guid CurrentCenterId { get; set; }
    }
}
