﻿using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Account.Queries.AutoLogin
{
    public class AutoLoginQuery : IRequest<AutoLoginVm>
    {
    }
    public class AutoLoginQueryHandler : IRequestHandler<AutoLoginQuery, AutoLoginVm>
    {
        private readonly ICurrentUserService _currentUserService;

        public AutoLoginQueryHandler(ICurrentUserService currentUserService)
        {
            _currentUserService = currentUserService;
        }

        public async Task<AutoLoginVm> Handle(AutoLoginQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser();
            if (user is null) throw new UnauthorizedAccessException();
            return await Task.FromResult(new AutoLoginVm()
            {
                UserName = user.UserName,
                Email = user.Email,
                IsAdmin = user.IsAdmin,
                CurrentCenterId = user.CurrentCenterId
            });
        }
    }

}
