﻿using EsmoServer.Application.Common.Exceptions;
using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Interfaces;
using EsmoServer.Infrastructure.Persistence;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Account.Queries.Login
{
    public class LoginQuery : IRequest<LoginVm>
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; }
    }
    public class LoginQueryHandler : IRequestHandler<LoginQuery, LoginVm>
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly SignInManager<UserItem> _signInManager;
        private readonly UserManager<UserItem> _userManager;
        private readonly IJwtGenerator _jwtGenerator;

        public LoginQueryHandler(UserManager<UserItem> userManager, SignInManager<UserItem> signInManager, ApplicationDbContext dbContext, IJwtGenerator jwtGenerator)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _dbContext = dbContext;
            _jwtGenerator = jwtGenerator;
        }

        public async Task<LoginVm> Handle(LoginQuery request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                throw new UnauthorizedAccessException();
            }
            var signInResult = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);
            var expires = request.Remember ? DateTime.Now.AddYears(1) : DateTime.Now.AddDays(1);
            if (signInResult.Succeeded)
            {
                return await Task.FromResult(new LoginVm
                {
                    AccessToken = _jwtGenerator.CreateToken(user, expires),
                    UserName = user.UserName,
                    Email = user.Email,
                    IsAdmin = user.IsAdmin,
                    CurrentCenterId = user.CurrentCenterId
                });
            }
            throw new UnauthorizedAccessException();
        }
    }
}
