﻿using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Interfaces.Gateways;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.Application.Account.Commands.SetCurrentCenter
{
    public class SetCurrentCenterCommand : IRequest
    {
        public string Id { get; set; }
    }
    public class SetCurrentCenterCommandHandler : IRequestHandler<SetCurrentCenterCommand>
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly ICentersGateway _centersGateway;
        private readonly IRolesGateway _rolesGateway;
        private readonly IUsersGateway _usersGateway;

        public SetCurrentCenterCommandHandler(ICurrentUserService currentUserService, ICentersGateway centersGateway, 
            IRolesGateway rolesGateway, IUsersGateway usersGateway)
        {
            _currentUserService = currentUserService;
            _centersGateway = centersGateway;
            _rolesGateway = rolesGateway;
            _usersGateway = usersGateway;
        }
        public Task<Unit> Handle(SetCurrentCenterCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser();
            if (user is null) throw new UnauthorizedAccessException();

            Guid centerId = Guid.Parse(request.Id);

            var center = _centersGateway.Where(x => x.Id == centerId).First();
            if (center == null) throw new KeyNotFoundException("Center not found");

            if (!IsUserAllowedToSetCenter(user, center)) throw new AccessViolationException();

            user.CurrentCenterId = center.Id;
            _usersGateway.SaveChanges();
            return Unit.Task;
        }
        private bool IsUserAllowedToSetCenter(UserItem user, CenterItem center)
        {
            if (user.IsAdmin || center.OwnerUserId == user.Id) return true;
            var roles = _rolesGateway.GetRolesByUser(user);
            foreach (var role in roles)
            {
                if (role.CenterId == center.Id) return true;
            }
            return false;
        }
    }
}
