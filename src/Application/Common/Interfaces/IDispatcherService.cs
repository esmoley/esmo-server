﻿using EsmoServer.Application.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Common.Interfaces
{
    public interface IDispatcherService
    {
        void DispatchCaller(DispatcherActionVm action);
    }
}
