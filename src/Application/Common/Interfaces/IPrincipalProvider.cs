﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Common.Interfaces
{
    public interface IPrincipalProvider
    {
        ClaimsPrincipal Principal { get; set; }
    }
}
