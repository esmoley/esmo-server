﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Common.Exceptions
{
    public class RestException : Exception
    {
        public RestException(HttpStatusCode statusCode):base(statusCode.ToString())
        {
        }
    }
}
