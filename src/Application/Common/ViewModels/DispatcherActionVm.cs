﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Common.ViewModels
{
    public class DispatcherActionVm
    {
        public string Method { get; set; }
        public object Payload { get; set; }

        public DispatcherActionVm(string method, object payload = null)
        {
            Method = method;
            Payload = payload;
        }
    }
}
