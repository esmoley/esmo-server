﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Application.Common.ViewModels
{
    public class AlertVm
    {
        public string Title { get; set; }
        public int Status { get; set; }
    }
}
