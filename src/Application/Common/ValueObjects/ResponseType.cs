﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValueOf;

namespace EsmoServer.Application.Common.ValueObjects
{
    public class ResponseType : ValueOf<string, ResponseType>
    {
        public static ResponseType Default = new ResponseType { Value = "default" };
        public static ResponseType Error = new ResponseType { Value = "error" };
        public static ResponseType Success = new ResponseType { Value = "success" };
        public static ResponseType Info = new ResponseType { Value = "info" };
        public static ResponseType Warning = new ResponseType { Value = "warning" };
    }
}
