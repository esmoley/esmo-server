﻿using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Infrastructure.Gateways
{
    public class RolesGateway : IRolesGateway
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public RolesGateway(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        public IEnumerable<RoleItem> GetRolesByUser(UserItem user)
        {
            var roleIds = _applicationDbContext.UsersRoles.Where(x => x.UserId == user.Id).Select(x => x.RoleId);
            return _applicationDbContext.Roles.Where(x => roleIds.Contains(x.Id));
        }
    }
}
