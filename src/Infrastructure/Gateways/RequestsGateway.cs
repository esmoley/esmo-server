﻿using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Filters;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Infrastructure.Gateways
{
    public class RequestsGateway : IRequestsGateway
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public RequestsGateway(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Create(RequestItem requestItem)
        {
            _applicationDbContext.Requests.Add(requestItem);
            _applicationDbContext.SaveChanges();
        }

        public void SaveChanges()
        {
            _applicationDbContext.SaveChanges();
        }
        public RequestItem Get(Guid id)
        {
            return _applicationDbContext.Requests.First(x => x.Id == id);
        }

        public IQueryable<RequestItem> Where(Expression<Func<RequestItem, bool>> predicate)
        {
            return _applicationDbContext.Requests.Where(predicate);
        }
    }
}
