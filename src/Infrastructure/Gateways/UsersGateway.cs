﻿using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Infrastructure.Gateways
{
    public class UsersGateway : IUsersGateway
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public UsersGateway(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IQueryable<UserItem> Where(Expression<Func<UserItem, bool>> predicate)
        {
            return _applicationDbContext.Users.Where(predicate);
        }
        public void SaveChanges()
        {
            _applicationDbContext.SaveChanges();
        }
    }
}
