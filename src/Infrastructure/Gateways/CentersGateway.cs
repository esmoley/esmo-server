﻿using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Interfaces.Gateways;
using EsmoServer.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EsmoServer.Infrastructure.Gateways
{
    public class CentersGateway : ICentersGateway
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IRolesGateway _rolesGateway;

        public CentersGateway(ApplicationDbContext applicationDbContext, IRolesGateway rolesGateway)
        {
            _applicationDbContext = applicationDbContext;
            _rolesGateway = rolesGateway;
        }

        public void Create(CenterItem center, UserItem user)
        {
            center.CreatedByUserId = user.Id;
            center.OwnerUserId = user.Id;
            _applicationDbContext.Add(center);
            //_applicationDbContext.UsersCenters.Add(new UserCentersRolesItem { CenterId = center.Id, UserId = user.Id });
            _applicationDbContext.SaveChanges();
        }

        public void Delete(CenterItem center)
        {
            /*var usersCenters = _applicationDbContext.Roles.Add.Where(x => x.CenterId == center.Id);
            if (usersCenters.Any())
            {
                _applicationDbContext.UsersCenters.RemoveRange(usersCenters);
            }*/
            _applicationDbContext.Remove(center);
            _applicationDbContext.SaveChanges();
        }

        public KeyValuePair<IEnumerable<RoleItem>, IEnumerable<CenterItem>> GetRolesCentersByUser(UserItem user)
        {
            var userRoles = _rolesGateway.GetRolesByUser(user);
            var centerIds = userRoles.Select(x => x.CenterId);
            if (user.IsAdmin)
                return new KeyValuePair<IEnumerable<RoleItem>, IEnumerable<CenterItem>>(userRoles, _applicationDbContext.Centers
                    .OrderByDescending(x=>x.OwnerUserId == user.Id)
                    .ThenByDescending(x=> centerIds.Contains(x.Id)).ToList());
            
            return new KeyValuePair<IEnumerable<RoleItem>, IEnumerable<CenterItem>>(userRoles, 
                _applicationDbContext.Centers.Where(x => centerIds.Contains(x.Id) || x.OwnerUserId == user.Id).ToList());
        }

        public IQueryable<CenterItem> Where(Expression<Func<CenterItem, bool>> predicate)
        {
            return _applicationDbContext.Centers.Where(predicate);
        }

        public void SaveChanges()
        {
            _applicationDbContext.SaveChanges();
        }
    }
}
