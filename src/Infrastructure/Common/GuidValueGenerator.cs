﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Infrastructure.Common
{
    public class GuidValueGenerator : ValueGenerator<Guid>
    {
        public override Guid Next(EntityEntry entry)
        {
            return RT.Comb.Provider.PostgreSql.Create();
        }

        public override bool GeneratesTemporaryValues => false;
    }
}
