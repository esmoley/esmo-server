﻿using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Interfaces.Gateways;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.Infrastructure.Persistence.Seeds
{
    public class ApplicationDbContextSeed
    {
        public static async Task SeedData(ApplicationDbContext context, UserManager<UserItem> userManager,
            ICentersGateway centersGateway)
        {
            if (!userManager.Users.Any())
            {
                var users = new List<UserItem>
                {
                    new UserItem
                    {
                        Email = "admin@test",
                        UserName = "Admin",
                        Password = "test12",
                        IsAdmin = true
                        //Role = "admin"
                    },
                    new UserItem
                    {
                        Email = "test@test",
                        UserName = "Test",
                        Password = "test12",
                        //Role = "user"
                    },
                    new UserItem
                    {
                        Email = "test2@test",
                        UserName = "Test2",
                        Password = "test12",
                        //Role = "user"
                    }
                };
                foreach (var user in users)
                {
                    var identityResult = await userManager.CreateAsync(user, user.Password);
                    if (!identityResult.Succeeded)
                    {
                        throw new Exception(identityResult.Errors.First().Description);
                    }
                }
                var centers = new List<CenterItem> { new CenterItem { Title = "TestCenter", Address = "Address", Phone = "777", Site = "www.testcenter.com" } };
                foreach (var center in centers)
                {
                    centersGateway.Create(center, users[1]);
                }
            }
        }
    }
}
