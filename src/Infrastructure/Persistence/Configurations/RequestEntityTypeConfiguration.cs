﻿using EsmoServer.Domain.Entities;
using EsmoServer.Infrastructure.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Infrastructure.Persistence.Configurations
{
    public class RequestEntityTypeConfiguration : IEntityTypeConfiguration<RequestItem>
    {
        public void Configure(EntityTypeBuilder<RequestItem> builder)
        {
            builder.ToTable("Requests");

            //builder.HasIndex(x => x.Type.Value)
            //builder.HasIndex(x => x.Status.Value);
            builder.Property(x => x.Id)
                .HasValueGenerator<GuidValueGenerator>()
                .ValueGeneratedOnAdd()
                .IsRequired();

            builder.OwnsOne(request => request.Status).HasIndex(x => x.Value);
            builder.OwnsOne(request => request.Status)
                .Property(x => x.Value)
                .HasColumnName("Status");

            builder.OwnsOne(request => request.Type).HasIndex(x => x.Value);
            builder.OwnsOne(request => request.Type)
                .Property(x => x.Value)
                .HasColumnName("Type");
        }
    }
}
