﻿using EsmoServer.Domain.Entities;
using EsmoServer.Infrastructure.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Infrastructure.Persistence.Configurations
{
    public class UserRoleEntityTypeConfiguration : IEntityTypeConfiguration<UserRoleItem>
    {
        public void Configure(EntityTypeBuilder<UserRoleItem> builder)
        {
            //builder.ToTable("Requests");

            //builder.HasIndex(x => x.Type.Value)
            //builder.HasIndex(x => x.Status.Value);
            builder.Property(x => x.Id)
                .HasValueGenerator<GuidValueGenerator>()
                .ValueGeneratedOnAdd()
                .IsRequired();
        }
    }
}
