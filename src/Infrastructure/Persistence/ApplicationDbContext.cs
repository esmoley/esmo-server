﻿using EsmoServer.Domain.Entities;
using EsmoServer.Infrastructure.Persistence.Configurations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace EsmoServer.Infrastructure.Persistence
{
    public class ApplicationDbContext : IdentityUserContext<UserItem, Guid>
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }
        public DbSet<CenterItem> Centers { get; set; }
        public DbSet<RequestItem> Requests { get; set; }
        public DbSet<RoleItem> Roles { get; set; }
        public DbSet<UserRoleItem> UsersRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new CenterEntityTypeConfiguration());
            builder.ApplyConfiguration(new RequestEntityTypeConfiguration());
            builder.ApplyConfiguration(new RoleEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserRoleEntityTypeConfiguration());
        }

    }
}
