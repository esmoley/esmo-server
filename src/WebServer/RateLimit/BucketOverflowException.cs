﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.RateLimit
{
    public class BucketOverflowException : Exception
    {
        public BucketOverflowException(string message) : base(message)
        {

        }
    }
}
