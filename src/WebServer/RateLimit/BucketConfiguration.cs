﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.RateLimit
{
    public class BucketConfiguration
    {
        public int MaxFill { get; set; }
        public TimeSpan LeakRateTimeSpan { get; set; }
        public int LeakRate { get; set; }
    }
}
