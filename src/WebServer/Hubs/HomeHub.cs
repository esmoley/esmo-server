﻿using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Hubs
{
    [Authorize]
    public partial class HomeHub : Hub
    {
        protected readonly ISender Mediator;
        //private readonly ICurrentUserService _currentUserService;
        protected readonly UserManager<UserItem> UserManager;

        public HomeHub(ISender mediator, UserManager<UserItem> userManager)
        {
            Mediator = mediator;
            UserManager = userManager;
        }

        /*public override async Task OnConnectedAsync()
        {
            //var response = await _rateThrottleProvider.ThrottleAsync(Context);
            //if (response != null)
            //await Clients.Caller.SendAsync("OnConnected", response);
            //await _mediator.Send(new OnConnectedRequest(this).ToOnConnectedCommand());
            await base.OnConnectedAsync();
        }*/
        
        public async Task Send()
        {
            await Clients.All.SendAsync("Receive", "ASFASFASFAF");
            var user = await UserManager.GetUserAsync(Context.User);
            //await Clients.All.SendAsync("Receive", user);
            await Clients.All.SendAsync("Receive", UserManager.GetUserId(Context.User));
        }
    }
}
