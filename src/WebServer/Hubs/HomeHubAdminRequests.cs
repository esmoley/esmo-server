﻿using EsmoServer.Application.Admin.Request.Commands.Process;
using EsmoServer.Application.Admin.Request.Queries.Get;
using EsmoServer.Application.Request.Commands.CreateCenterRequest;
using EsmoServer.Application.Request.Commands.UpdateCenterRequest;
using EsmoServer.Application.Request.Queries.GetCreateCenterRequests;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Hubs
{
    public partial class HomeHub : Hub
    {
        public async Task<GetAdminRequestsVm> GetAdminRequests()
        {
            return await Mediator.Send(new GetAdminRequestsQuery(), Context.ConnectionAborted);
        }
        public async Task ProcessAdminRequests(ProcessAdminRequestCommand request)
        {
            await Mediator.Send(request, Context.ConnectionAborted);
        }
    }
}
