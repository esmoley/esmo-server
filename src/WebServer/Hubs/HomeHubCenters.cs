﻿using EsmoServer.Application.Center.Queries.GetCenters;
using EsmoServer.Application.Request.Queries.GetCreateCenterRequests;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Hubs
{
    public partial class HomeHub : Hub
    {
        public async Task<GetCentersVm> GetCenters()
        {
            return await Mediator.Send(new GetCentersQuery(), Context.ConnectionAborted);
        }
    }
}
