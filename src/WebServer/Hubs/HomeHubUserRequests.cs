﻿using EsmoServer.Application.Request.Commands.CreateCenterRequest;
using EsmoServer.Application.Request.Commands.UpdateCenterRequest;
using EsmoServer.Application.Request.Queries.GetCreateCenterRequests;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Hubs
{
    public partial class HomeHub : Hub
    {
        public async Task<GetCreateCenterRequestsVm> GetCreateCenterRequests()
        {
            return await Mediator.Send(new GetCreateCenterRequestsQuery(), Context.ConnectionAborted);
            //await Clients.Caller.SendAsync("GetCreateCenterRequests", response);
        }
        public async Task AddCreateCenterRequest(CreateCenterRequestCommand request)
        {
            await Mediator.Send(request, Context.ConnectionAborted);
        }

        public async Task UpdateCreateCenterRequest(UpdateCreateCenterRequestCommand request)
        {
            await Mediator.Send(request, Context.ConnectionAborted);
        }
    }
}
