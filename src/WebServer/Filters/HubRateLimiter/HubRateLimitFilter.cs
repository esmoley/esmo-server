﻿using EsmoServer.WebServer.RateLimit;
using Microsoft.AspNetCore.Http.Connections.Features;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Filters.HubRateLimit
{
    public class HubRateLimitFilter : IHubFilter
    {
        private static BucketConfiguration _connectionRateLimitConfiguration = new BucketConfiguration
        {
            LeakRate = 2,
            LeakRateTimeSpan = TimeSpan.FromMinutes(1),
            MaxFill = 20
        };
        private static ConcurrentDictionary<string, LeakyBucket> _currentItems = new ConcurrentDictionary<string, LeakyBucket>();
        private readonly ILogger<HubRateLimitFilter> _logger;

        public HubRateLimitFilter(ILogger<HubRateLimitFilter> logger)
        {
            _logger = logger;
        }

        public async ValueTask<object> InvokeMethodAsync(
            HubInvocationContext invocationContext, Func<HubInvocationContext, ValueTask<object>> next)
        {
            _logger.LogInformation($"Calling hub method'{invocationContext.HubMethodName}'");

            return await next(invocationContext);
        }

        // Optional method
        public Task OnConnectedAsync(HubLifetimeContext context, Func<HubLifetimeContext, Task> next)
        {
            //Console.WriteLine(context.Context.Features.Get<IHttpContextFeature>().HttpContext.Request.Cookies["language"]); 
            //var context2 = context.Context.GetHttpContext();
            //var cookies = context2.Request.Cookies;
            //context.Context.Request

            _logger.LogInformation($"OnConnectedAsync");
            var ip = GetIp(context);
            if (ip != null)
            {
                _logger.LogInformation($"ip is {ip}");
                var key = ip + "OnConnectedAsync";
                if (!_currentItems.TryGetValue(key, out var currentItem))
                {
                    currentItem = new LeakyBucket(_connectionRateLimitConfiguration);
                    _currentItems.TryAdd(key, currentItem);
                }
                currentItem.GainAccessAsync().Wait();
                _logger.LogInformation($"access gained for ip {ip}");
            }
            else
            {
                _logger.LogWarning($"ip is null. Bypassing");
            }

            return next(context);
        }

        // Optional method
        public Task OnDisconnectedAsync(
            HubLifetimeContext context, Exception exception, Func<HubLifetimeContext, Exception, Task> next)
        {
            _logger.LogInformation($"OnDisconnectedAsync");
            return next(context, exception);
        }
        private string GetIp(HubLifetimeContext context)
        {
            return context.Context.GetHttpContext().Connection.RemoteIpAddress?.ToString();
        }
    }
}
