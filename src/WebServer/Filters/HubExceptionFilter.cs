﻿using EsmoServer.WebServer.RateLimit;
using FluentValidation;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Filters
{
    public class HubExceptionFilter : IHubFilter
    {
        private readonly ILogger<HubExceptionFilter> _logger;

        public HubExceptionFilter(ILogger<HubExceptionFilter> logger)
        {
            _logger = logger;
        }

        public async ValueTask<object> InvokeMethodAsync(
            HubInvocationContext invocationContext, Func<HubInvocationContext, ValueTask<object>> next)
        {
            _logger.LogInformation($"Calling hub method '{invocationContext.HubMethodName}'");
            try
            {
                return await next(invocationContext);
            }
            catch(ValidationException ex)
            {
                _logger.LogError($"ValidationException calling '{invocationContext.HubMethodName}': {ex}");
                if (ex.Errors!=null && ex.Errors.Count() > 0)
                {
                    return new { error = string.Join(" ", ex.Errors.Select(x => x.ErrorMessage)) };
                }
                return new { error = ex.Message };
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception calling '{invocationContext.HubMethodName}': {ex}");
                //await invocationContext.Hub.Clients.Caller.SendAsync("Error", ex.Message);
                //return null;
                //throw;
                return new { error = ex.Message };
            }
        }

        // Optional method
        public Task OnConnectedAsync(HubLifetimeContext context, Func<HubLifetimeContext, Task> next)
        {
            try
            {
                _logger.LogInformation($"OnConnectedAsync");
                return next(context);
            }
            catch(Exception ex)
            {
                _logger.LogError($"OnConnectedAsync Error", ex.Message);
                context.Hub.Clients.Caller.SendAsync("Error", ex.Message).Wait();
                throw;
            }
        }

        // Optional method
        public Task OnDisconnectedAsync(
            HubLifetimeContext context, Exception exception, Func<HubLifetimeContext, Exception, Task> next)
        {
            _logger.LogInformation($"OnDisconnectedAsync");
            return next(context, exception);
        }
    }
}