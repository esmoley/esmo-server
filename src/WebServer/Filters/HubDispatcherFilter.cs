﻿using EsmoServer.WebServer.Services;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Filters
{
    public class HubDispatcherFilter : IHubFilter
    {
        private readonly ILogger<HubDispatcherFilter> _logger;
        private readonly DispatcherService _homeDispatcherService;

        public HubDispatcherFilter(ILogger<HubDispatcherFilter> logger, DispatcherService homeDispatcherService)
        {
            _logger = logger;
            _homeDispatcherService = homeDispatcherService;
        }
        public async ValueTask<object> InvokeMethodAsync(
            HubInvocationContext invocationContext, Func<HubInvocationContext, ValueTask<object>> next)
        {
            _homeDispatcherService.Resolve(invocationContext.Hub);
            return await next(invocationContext);
        }
    }
}
