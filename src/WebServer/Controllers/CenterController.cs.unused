﻿using EsmoServer.Application.Account.Queries.AutoLogin;
using EsmoServer.Application.Account.Queries.Login;
using EsmoServer.Application.Request.Commands;
using EsmoServer.Application.Request.Commands.CreateCenterRequest;
using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Entities;
using EsmoServer.Infrastructure.Identity;
using EsmoServer.Infrastructure.Persistence;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using EsmoServer.Application.Request.Queries.GetCreateCenterRequests;
using EsmoServer.Application.Request.Commands.UpdateCenterRequest;
using EsmoServer.Application.Center.Queries.GetCenters;

namespace EsmoServer.WebServer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class CenterController : ControllerBase
    {
        private readonly ISender _mediator;
        public CenterController(ISender mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<GetCentersVm> Get(CancellationToken cancellationToken)
        {
            return await _mediator.Send(new GetCentersQuery(), cancellationToken);
        }
    }
}
