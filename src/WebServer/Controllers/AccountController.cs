﻿using EsmoServer.Application.Account.Queries.AutoLogin;
using EsmoServer.Application.Account.Queries.Login;
using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Entities;
using EsmoServer.Infrastructure.Identity;
using EsmoServer.Infrastructure.Persistence;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class AccountController : ControllerBase
    {
        private readonly ISender _mediator;
        private readonly UserManager<UserItem> _userManager;
        private readonly SignInManager<UserItem> _signInManager;
        private readonly ICurrentUserService _currentUserService;
        public AccountController(ISender mediator, UserManager<UserItem> userManager, SignInManager<UserItem> signInManager, ICurrentUserService currentUserService)
        {
            _mediator = mediator;
            _userManager = userManager;
            _signInManager = signInManager;
            _currentUserService = currentUserService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<LoginVm> Login(LoginQuery request, CancellationToken cancellationToken)
        {
            return await _mediator.Send(request, cancellationToken);
        }

        [HttpGet]
        public async Task<AutoLoginVm> AutoLogin(CancellationToken cancellationToken)
        {
            return await _mediator.Send(new AutoLoginQuery(), cancellationToken);
        }
    }
}
