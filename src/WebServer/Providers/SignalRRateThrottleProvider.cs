﻿using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Providers
{
    public enum ThrottleOption
    {
        /// <summary>
        /// Default behavior, returns a BadRequestResponse when throttling occurs
        /// </summary>
        None,
        /// <summary>
        /// Throw an exception when throttling occurs
        /// </summary>
        ThrowOnError
    }
    public class ThrottledException:Exception{
        public ThrottledException(string message) : base(message)
        {

        }
    }
    /// <summary>
    /// SignalR rate throttling provider
    /// </summary>
    public class SignalRRateThrottleProvider
    {
        private IpRateLimitOptions _options;
        private IIpAddressParser _ipParser;
        private IpRateLimitProcessor _processor;
        private ILogger<SignalRRateThrottleProvider> _logger;

        /// <summary>
        /// Create a SignalR rate throttling provider
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="rateLimit"></param>
        /// <param name="options"></param>
        /// <param name="ipParser"></param>
        public SignalRRateThrottleProvider(ILogger<SignalRRateThrottleProvider> logger, IpRateLimitProcessor rateLimit, IOptions<IpRateLimitOptions> options, IIpAddressParser ipParser)
        {
            _logger = logger;
            _options = options != null ? options.Value : null;
            _ipParser = ipParser;
            _processor = rateLimit;
        }

        /// <summary>
        /// Perform rate throttling on a SignalR request
        /// </summary>
        /// <param name="callerContext"></param>
        /// <returns></returns>
        public async Task<IActionResult> ThrottleAsync(HubCallerContext callerContext)
        {
            return await ThrottleAsync(callerContext, ThrottleOption.None);
        }

        /// <summary>
        /// Perform rate throttling on a SignalR request
        /// </summary>
        /// <param name="callerContext"></param>
        /// <param name="options">The throttle options to use</param>
        /// <returns></returns>
        public async Task<IActionResult> ThrottleAsync(HubCallerContext callerContext, ThrottleOption options)
        {
            if (callerContext != null)
            {
                var context = callerContext.GetHttpContext();
                var identity = SetIdentity(context);
                if (_processor.IsWhitelisted(identity))
                {
                    // allow through
                    return null;
                }

                var rules = await _processor.GetMatchingRulesAsync(identity);
                return await ProcessRulesAsync(rules, identity, callerContext.GetHttpContext(), options);
            }

            // no context available, allow through
            return null;
        }

        private async Task<IActionResult> ProcessRulesAsync(IEnumerable<RateLimitRule> rules, ClientRequestIdentity identity, HttpContext httpContext, ThrottleOption options)
        {
            foreach (var rule in rules)
            {
                if (rule.Limit > 0)
                {
                    // increment counter
                    var counter = await _processor.ProcessRequestAsync(identity, rule);

                    // check if key expired
                    if (counter.Timestamp + rule.PeriodTimespan.Value < DateTime.UtcNow)
                    {
                        continue;
                    }

                    // check if limit is reached
                    if (counter.Count > rule.Limit)
                    {
                        //compute retry after value
                        var retryAfter = _processor.GetRateLimitHeaders(counter, rule);

                        // log blocked request
                        LogBlockedRequest(httpContext, identity, counter, rule);

                        // break execution
                        return await ReturnQuotaExceededResponseAsync(httpContext, rule, retryAfter.Remaining, options);
                    }
                }
                // if limit is zero or less, block the request.
                else
                {
                    // process request count
                    var counter = await _processor.ProcessRequestAsync(identity, rule);

                    // log blocked request
                    LogBlockedRequest(httpContext, identity, counter, rule);

                    // break execution (Int32 max used to represent infinity)
                    return await ReturnQuotaExceededResponseAsync(httpContext, rule, Int32.MaxValue.ToString(System.Globalization.CultureInfo.InvariantCulture), options);
                }
            }

            return null;
        }

        private ClientRequestIdentity SetIdentity(HttpContext httpContext)
        {
            var clientId = "anon";
            if (httpContext.Request.Headers.Keys.Contains(_options.ClientIdHeader, StringComparer.CurrentCultureIgnoreCase))
            {
                clientId = httpContext.Request.Headers[_options.ClientIdHeader].First();
            }

            var clientIp = string.Empty;
            try
            {
                var ip = _ipParser.GetClientIp(httpContext);
                if (ip == null)
                {
                    throw new Exception("IpRateLimitMiddleware can't parse caller IP");
                }

                clientIp = ip.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("IpRateLimitMiddleware can't parse caller IP", ex);
            }

            return new ClientRequestIdentity
            {
                ClientIp = clientIp,
                Path = httpContext.Request.Path.ToString().ToLowerInvariant(),
                HttpVerb = httpContext.Request.Method.ToLowerInvariant(),
                ClientId = clientId
            };
        }

        private async Task<IActionResult> ReturnQuotaExceededResponseAsync(HttpContext httpContext, RateLimitRule rule, string retryAfter, ThrottleOption options)
        {
            var message = string.IsNullOrEmpty(_options.QuotaExceededMessage) ? $"API calls quota exceeded! Maximum admitted {rule.Limit} per {rule.Period}." : _options.QuotaExceededMessage;
            var ex = new ThrottledException(message);
            if (options.HasFlag(ThrottleOption.ThrowOnError))
                throw ex;

            return ApiBadRequestResponse(ex);
        }

        private IActionResult ApiBadRequestResponse(ThrottledException ex)
        {
            throw new NotImplementedException();
        }

        private void LogBlockedRequest(HttpContext httpContext, ClientRequestIdentity identity, RateLimitCounter counter, RateLimitRule rule)
        {
            _logger.LogInformation($"Request {identity.HttpVerb}:{identity.Path} from IP {identity.ClientIp} has been blocked, quota {rule.Limit}/{rule.Period} exceeded by {counter.Count}. Blocked by rule {rule.Endpoint}, TraceIdentifier {httpContext.TraceIdentifier}.");
        }
    }
}
