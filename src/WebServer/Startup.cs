using AspNetCoreRateLimit;
using EsmoServer.Application;
using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Entities;
using EsmoServer.Infrastructure;
using EsmoServer.Infrastructure.Persistence;
using EsmoServer.WebServer.Filters;
using EsmoServer.WebServer.Filters.HubRateLimit;
using EsmoServer.WebServer.Hubs;
using EsmoServer.WebServer.Services;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.WebServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AllowAllPolicy", builder =>
            {
                builder.SetIsOriginAllowed(origin => true)
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            }));

            // needed to load configuration from appsettings.json
            services.AddOptions();

            // needed to store rate limit counters and ip rules
            services.AddMemoryCache();
            // inject counter and rules stores
            services.AddInMemoryRateLimiting();
            // configuration (resolvers, counter key builders)

            //load general configuration from appsettings.json
            services.Configure<IpRateLimitOptions>(Configuration.GetSection("IpRateLimiting"));

            //load ip rules from appsettings.json
            services.Configure<IpRateLimitPolicies>(Configuration.GetSection("IpRateLimitPolicies"));
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();


            services.AddApplication();
            services.AddInfrastructure(Configuration);
            services.AddControllers(options =>
            {
                options.EnableEndpointRouting = false;
                var policy = new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme).RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));
                options.Filters.Add(new ApiExceptionFilterAttribute());
            }).SetCompatibilityVersion(CompatibilityVersion.Latest)
            .AddFluentValidation();

            services.AddSwaggerGen(c =>
            {
                c.OperationFilter<AuthorizationHeaderParameterOperationFilter>();
                c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Scheme = "bearer"
                });
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebServer", Version = "v1" });
            });
            services.AddSignalR(options =>
            {
                options.AddFilter<HubLanguageFilter>();
                options.AddFilter<HubRateLimitFilter>();
                options.AddFilter<HubExceptionFilter>();
                options.AddFilter<HubDispatcherFilter>();
                options.EnableDetailedErrors = true;
            });

            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<DispatcherService>();
            services.AddScoped<IDispatcherService>(x => x.GetRequiredService<DispatcherService>());
            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebServer v1"));
            }
            app.UseCors("AllowAllPolicy");
            app.UseIpRateLimiting();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<HomeHub>("/hubs/home");//.RequireAuthorization(JwtBearerDefaults.AuthenticationScheme);
            });
        }
    }
}
