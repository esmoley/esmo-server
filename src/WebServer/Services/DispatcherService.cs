﻿using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Application.Common.ViewModels;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Services
{
    public class DispatcherService : IDispatcherService
    {
        private Hub _hub;
        public void Resolve(Hub hub)
        {
            _hub = hub;
        }
        public void DispatchCaller(DispatcherActionVm action)
        {
            _hub.Clients.Caller.SendAsync(action.Method, action.Payload);
        }
    }
}
