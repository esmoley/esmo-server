﻿using EsmoServer.Application.Common.Interfaces;
using EsmoServer.Domain.Entities;
using EsmoServer.WebServer.Hubs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EsmoServer.WebServer.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<UserItem> _userManager;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor, UserManager<UserItem> userManager, IHubContext<HomeHub> hubContext)
        {
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        public string UserId => _httpContextAccessor.HttpContext?.User?.FindFirstValue(JwtRegisteredClaimNames.Sub);
        public string UserName => _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.Name);
        public UserItem GetUser()
        {
            return _userManager.GetUserAsync(_httpContextAccessor.HttpContext?.User).GetAwaiter().GetResult();
        }
    }
}
