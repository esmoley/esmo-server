﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValueOf;

namespace EsmoServer.Domain.ValueObjects
{
    public class RequestType : ValueOf<string, RequestType>
    {
        public static readonly RequestType CreateCenter = new RequestType { Value = "CreateCenter" };
    }
}
