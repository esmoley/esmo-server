﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValueOf;

namespace EsmoServer.Domain.ValueObjects
{
    public class CenterStatus : ValueOf<string, RequestStatus>
    {
        public static readonly CenterStatus OK = new CenterStatus { Value = "OK" };
        public static readonly CenterStatus WaitingPayment = new CenterStatus { Value = "WaitingPayment" };
    }
}
