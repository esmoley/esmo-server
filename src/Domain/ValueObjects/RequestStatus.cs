﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValueOf;

namespace EsmoServer.Domain.ValueObjects
{
    public class RequestStatus : ValueOf<string, RequestStatus>
    {
        public static readonly RequestStatus Pending = new RequestStatus { Value = "Pending" };
        public static readonly RequestStatus Denied = new RequestStatus { Value = "Denied" };
        public static readonly RequestStatus Accepted = new RequestStatus { Value = "Accepted" };
        public static IEnumerable<RequestStatus> GetAll()
        {
            yield return Pending;
            yield return Denied;
            yield return Accepted;
        }
    }
}
