﻿using EsmoServer.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Domain.Filters
{
    public class RequestFilter
    {
        public IEnumerable<string> UserIds { get; set; }
        public IEnumerable<RequestType> Types { get; set; }
        public IEnumerable<RequestStatus> Status { get; set; }
    }
}
