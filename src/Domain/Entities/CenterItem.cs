﻿using EsmoServer.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Domain.Entities
{
    [Index(nameof(CreatedByUserId))]
    [Index(nameof(OwnerUserId))]
    [Index(nameof(Title), IsUnique = true)]
    public class CenterItem
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string Site { get; set; }

        [Required]
        public Guid OwnerUserId { get; set; }

        [Required]
        public Guid CreatedByUserId { get; set; }

        public DateTime? CreatedAt { get; set; } = DateTime.Now;

        public DateTime? UpdatedAt { get; set; } = DateTime.Now;

        public CenterStatus Status { get; set; } = CenterStatus.OK;
    }
}
