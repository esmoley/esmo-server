﻿using EsmoServer.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Domain.Entities
{
    [Index(nameof(RequestedByUserId))]
    public class RequestItem
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [Column(TypeName = "jsonb")]
        public string Data { get; set; }

        [Required]
        public Guid RequestedByUserId { get; set; }

        [Required]
        public RequestType Type { get; set; }

        [Required]
        public RequestStatus Status { get; set; } = RequestStatus.Pending;

        /// <summary>
        /// Admin that processed the request
        /// </summary>
        public Guid ProcessedByAdminId { get; set; }
        public string StatusReason { get; set; }

        public DateTime? CreatedAt { get; set; } = DateTime.Now;

        public DateTime? UpdatedAt { get; set; } = DateTime.Now;
    }
}
