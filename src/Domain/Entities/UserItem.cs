﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Domain.Entities
{
    [Index(nameof(IsAdmin))]
    public class UserItem : IdentityUser<Guid>
    {
        //public new string Id { get; set; }
        public string Password { get; set; }

        public Guid CurrentCenterId { get; set; }
        public bool IsAdmin { get; set; }
        public DateTime? CreatedAt { get; set; } = DateTime.Now;
        public DateTime? UpdatedAt { get; set; } = DateTime.Now;

    }
}
