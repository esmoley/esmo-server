﻿using EsmoServer.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Domain.Interfaces.Gateways
{
    public interface ICentersGateway
    {
        KeyValuePair<IEnumerable<RoleItem>, IEnumerable<CenterItem>> GetRolesCentersByUser(UserItem user);
        void Create(CenterItem center, UserItem user);
        void Delete(CenterItem center);
        IQueryable<CenterItem> Where(Expression<Func<CenterItem, bool>> predicate);
        void SaveChanges();
    }
}
