﻿using EsmoServer.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Domain.Interfaces.Gateways
{
    public interface IUsersGateway
    {
        IQueryable<UserItem> Where(Expression<Func<UserItem, bool>> predicate);
        void SaveChanges();
    }
}
