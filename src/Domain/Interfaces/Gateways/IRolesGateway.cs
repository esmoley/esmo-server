﻿using EsmoServer.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Domain.Interfaces.Gateways
{
    public interface IRolesGateway
    {
        IEnumerable<RoleItem> GetRolesByUser(UserItem user);
    }
}
