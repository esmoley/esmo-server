﻿using EsmoServer.Domain.Entities;
using EsmoServer.Domain.Filters;
using EsmoServer.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EsmoServer.Domain.Interfaces.Gateways
{
    public interface IRequestsGateway
    {
        void Create(RequestItem requestItem);
        void SaveChanges();
        RequestItem Get(Guid id);
        IQueryable<RequestItem> Where(Expression<Func<RequestItem, bool>> predicate);
    }
}
